﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceSpecificBoardSize : MonoBehaviour
{

    void Start()
    {
        AdjustScaleBasedOnScreenWidth();
    }

    void AdjustScaleBasedOnScreenWidth()
    {
        {
            float height = Camera.main.orthographicSize * 2;

            float width = Camera.main.aspect * height;

            float scaleX = (width * 0.8f) / GetComponent<SpriteRenderer>().bounds.size.x;

            transform.localScale = Vector3.one * scaleX;
            
        }
    }
}

