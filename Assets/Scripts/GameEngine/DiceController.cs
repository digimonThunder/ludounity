﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceController : MonoBehaviour
{
    [SerializeField]
    private Sprite[] m_DiceSprites;

    [SerializeField]
    private Animator m_DiceAnimation;

    [SerializeField]
    private SpriteRenderer m_DiceSpriteRenderer;

    private int m_MinValue = 1;
    public int m_MaxValue = 6;
    private bool m_CanDice;
    private PawnType m_PawnType;

    public bool CanDice
    {
        set
        {
            m_CanDice = value;
        }
    }

    void Awake()
    {
        m_DiceAnimation.enabled = false;
    }


    public void SetUp(PawnType inType)
    {
        m_PawnType = inType;
    }

    void OnMouseDown()
    {
        if (m_CanDice)
        {
            m_CanDice = false;

            RollDice();
        }
    }

    bool alterNate = false;

    private void RollDice()
    {
        int diceValue = Random.Range(m_MinValue, m_MaxValue + 1);

        // GameEventManager.Instance.TriggerDiceRolled(m_PawnType,diceValue);

        StartCoroutine(playDiceAnimation(diceValue));

    }

    public void BotRollDice(int inDiceValue)
    {
        StartCoroutine(playDiceAnimation(inDiceValue));

    }

    IEnumerator playDiceAnimation(int inDiceValue)
    {
        m_DiceAnimation.enabled = true;

        yield return new WaitForSeconds(0.5f);

        m_DiceAnimation.enabled = false;

        m_DiceSpriteRenderer.sprite = m_DiceSprites[inDiceValue - 1];

        GameEventManager.Instance.TriggerDiceRolled(m_PawnType, inDiceValue);
    }

    public IEnumerator UpdateDice(int inDiceValue)
    {
         m_DiceAnimation.enabled = true;

        yield return new WaitForSeconds(0.5f);

        m_DiceAnimation.enabled = false;

        m_DiceSpriteRenderer.sprite = m_DiceSprites[inDiceValue - 1];

        m_DiceSpriteRenderer.sprite = m_DiceSprites[inDiceValue - 1];
    }
}
