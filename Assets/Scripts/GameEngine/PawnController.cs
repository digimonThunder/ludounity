﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PawnState
{
    eLocked,
    eUnlocked,
    eFinish
}

public class PawnController : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer m_PawnSpriteRenderer;

    [SerializeField]
    private SpriteRenderer m_PawnBaseSpriteRenderer;

    [SerializeField]
    private Animator m_PawnSelectionIndicator;

    private PawnType m_PawnType;

    private PawnState m_CurrentState = PawnState.eLocked;

    private int m_CurrentBoardCellIndex = -1;

    private int m_BoardSlotIndex = -1;

    private List<BoardCell> m_PawnTrack;

    private bool m_CanTap = false;

    public PawnState CurrentPawnState
    {
        get
        {
            return m_CurrentState;
        }
    }

    public PawnType PawnType
    {
        get
        {
            return m_PawnType;
        }
    }

    public BoardCell CureentBoardCell
    {
        get
        {
            return m_PawnTrack[m_CurrentBoardCellIndex];
        }
    }

    public int BoardSlotIndex
    {
        get
        {
            return m_BoardSlotIndex;
        }
    }


    public void SetUpPawn(PawnType inPawnType, int inBoardSlotIndex)
    {
        m_PawnType = inPawnType;

        m_BoardSlotIndex = inBoardSlotIndex;

        m_PawnSpriteRenderer.sprite = GameAssetController.Instance.GetPawnSpriteForType(inPawnType);

        m_PawnBaseSpriteRenderer.sprite = GameAssetController.Instance.GetPawnBaseSpriteForType(inPawnType);

        m_PawnTrack = GameTurnManager.Instance.Board.GetPawnTrackByType(m_PawnType);
    }

    public void UnlockPawn()
    {
        ChangePawnState(PawnState.eUnlocked);
    }

    public void LockPawn()
    {
        ChangePawnState(PawnState.eLocked);
    }

    private bool SelectionIndicator
    {
        set
        {
            if (value)
            {
                // play selection animation on this token
                m_PawnSelectionIndicator.SetTrigger("Landed");
            }
            else
            {
                // stop selection animation on this token
                m_PawnSelectionIndicator.SetTrigger("Idle");
            }
        }
    }

    private void ChangePawnState(PawnState inState)
    {
        if (m_CurrentState != inState)
        {
            m_CurrentState = inState;

            if (inState == PawnState.eUnlocked)
            {
                m_CurrentBoardCellIndex = 0; //just out from board house 

                ///TOBE move pawn from inhouse to home star

                MoveForwardBy(0);
            }

            Debug.Log("Pawn State Changed to --> " + m_CurrentState);

        }
    }

    public void MoveForwardBy(int inValue)
    {
        if (m_CurrentState == PawnState.eUnlocked)
        {
            BoardCell currentBoardCell = m_PawnTrack[m_CurrentBoardCellIndex]; //removing from old cell

            currentBoardCell.UnRegisterPawnFromCell(this);


            int nextIndex = m_CurrentBoardCellIndex + inValue;

            Debug.Log(m_PawnTrack.Count + "--Next Box Index--" + nextIndex);


            if (nextIndex <= m_PawnTrack.Count - 1)
            {
                bool isLastCell = nextIndex == m_PawnTrack.Count - 1;

                if (isLastCell)
                {
                    ChangePawnState(PawnState.eFinish);
                }

                m_CurrentBoardCellIndex = nextIndex;


                BoardCell newBoardCell = m_PawnTrack[m_CurrentBoardCellIndex]; //adding to new cell

                newBoardCell.RegisterPawnToCell(this);


                transform.position = newBoardCell.transform.position; //pawn to diced board cell
            }
            else
            {

            }

        }
    }

    public void UpdateTapStatus(bool inState)
    {
        if (m_CanTap != inState)
        {
            SelectionIndicator = inState;
        }

        m_CanTap = m_CurrentState != PawnState.eFinish && inState;

    }

    public bool CanMoveForwardBy(int inDiceValue)
    {
        bool retVal = false;

        int nextIndex = m_CurrentBoardCellIndex + inDiceValue;

        if (nextIndex <= m_PawnTrack.Count - 1)
        {
            retVal = true;
        }

        return retVal;
    }

    void OnMouseDown()
    {
        if (m_CanTap)
        {
            GameEventManager.Instance.TriggerPawnSelected(this);
        }
    }
}
