﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager
{
    private int m_MaxPlayers;

    private PawnType m_SelectedPawn = PawnType.eRed;

    private GameMode m_SelectedMode = GameMode.eLocalMode;


    private string m_PlayerName = "";


    public PawnType SelectedPawn
    {
        get
        {
            return m_SelectedPawn;
        }
        set
        {
            m_SelectedPawn = value;
        }
    }

    public string PlayerName
    {
        get
        {
            return m_PlayerName;
        }
        set
        {
            m_PlayerName = value;
        }
    }


    public GameMode SelectedGameMode
    {
        get
        {
            return m_SelectedMode;
        }
        set
        {
            m_SelectedMode = value;
        }
    }

    public int MaxPlayers
    {
        get
        {

            if (m_MaxPlayers < 2)
            {
                m_MaxPlayers = 2;
            }
            else
            {

            }

            return m_MaxPlayers;

        }
        set
        {
            m_MaxPlayers = value;
        }
    }

    private static PlayerManager m_GlobalRef;

    private PlayerManager()
    {

    }

    public static PlayerManager Instance
    {
        get
        {
            if (m_GlobalRef == null)
            {
                m_GlobalRef = new PlayerManager();
            }

            return m_GlobalRef;
        }
    }

    public void Destroy()
    {
        m_GlobalRef = null;
    }
}
