﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PawnData
{
    public PawnType m_PawnType;

    public Sprite m_PawnSprite;

    public Sprite m_PawnBaseSprite;

}

public class GameAssetController : MonoBehaviour
{
    private static GameAssetController s_GlobalRef;

    [SerializeField]
    private PawnData[] m_PawnData;

    [SerializeField]
    private GameObject m_PawnPrefab;

    public GameObject PawnPrefab
    {
        get
        {
            return m_PawnPrefab;
        }
    }


    public static GameAssetController Instance
    {
        get
        {
            return s_GlobalRef;
        }
    }

    void Awake()
    {
        if (s_GlobalRef == null)
        {
            s_GlobalRef = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public Sprite GetPawnSpriteForType(PawnType inPawn)
    {
        Sprite retVal = null;

        for (int j = 0; j < m_PawnData.Length; j++)
        {
            if (m_PawnData[j].m_PawnType == inPawn)
            {
                retVal = m_PawnData[j].m_PawnSprite;
                break;
            }
        }

        return retVal;
    }

    public Sprite GetPawnBaseSpriteForType(PawnType inPawn)
    {
        Sprite retVal = null;

        for (int j = 0; j < m_PawnData.Length; j++)
        {
            if (m_PawnData[j].m_PawnType == inPawn)
            {
                retVal = m_PawnData[j].m_PawnBaseSprite;
                break;
            }
        }

        return retVal;
    }
}
