﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class GameSceneManager : MonoBehaviourPunCallbacks
{
    private PawnType m_SelectedPawnType = PawnType.eRed;

    private GameMode m_SelectedMode = GameMode.eLocalMode;

    private int m_MaxPlayers = 2;

    [PunRPC]
    private bool m_ShouldEndGame = false;

    [SerializeField]
    private GameTurnManager m_GameTurnManager;


    void Start()
    {
        m_SelectedMode = PlayerManager.Instance.SelectedGameMode;

        m_SelectedPawnType = PlayerManager.Instance.SelectedPawn;

        m_MaxPlayers = PlayerManager.Instance.MaxPlayers;

        GameTurnManager.Instance = m_GameTurnManager;

        CreatePlayers();
    }

    void CreatePlayers()
    {
        switch (m_SelectedMode)
        {
            case GameMode.eLocalMode:

                CreateLocalPlayers();

                break;

            case GameMode.eComputerMode:

                CreateComputerModePlayers();

                break;

            case GameMode.eGlobalMode:

                CreateGlobalModePlayers();

                break;
        }
    }

    void CreateLocalPlayers()
    {
        List<IPlayer> players = new List<IPlayer>();


        if (m_MaxPlayers == 2)
        {
            IPlayer playerRed = new LocalPlayer();
            playerRed.SetUpPlayer(PlayerManager.Instance.PlayerName, PawnType.eRed, PlayerType.ePlayer, 1);

            IPlayer playerYellow = new LocalPlayer();
            playerYellow.SetUpPlayer("Player_2", PawnType.eYellow, PlayerType.ePlayer, 2);

            players.Add(playerRed);
            players.Add(playerYellow);

        }
        else
        {
            IPlayer playerRed = new LocalPlayer();
            playerRed.SetUpPlayer(PlayerManager.Instance.PlayerName, PawnType.eRed, PlayerType.ePlayer, 1);

            IPlayer playerGreen = new LocalPlayer();
            playerGreen.SetUpPlayer("Player_2", PawnType.eGreen, PlayerType.ePlayer, 2);


            IPlayer playerYellow = new LocalPlayer();
            playerYellow.SetUpPlayer("Player_3", PawnType.eYellow, PlayerType.ePlayer, 3);

            IPlayer playerBlue = new LocalPlayer();
            playerBlue.SetUpPlayer("Player_4", PawnType.eBlue, PlayerType.ePlayer, 4);

            players.Add(playerRed);
            players.Add(playerGreen);
            players.Add(playerYellow);
            players.Add(playerBlue);

        }

        m_GameTurnManager.SetUpGamePlay(players, GameMode.eLocalMode);
    }

    void CreateComputerModePlayers()
    {
        List<IPlayer> players = new List<IPlayer>();

        if (m_MaxPlayers == 2)
        {

            IPlayer playerRed = new LocalPlayer();
            playerRed.SetUpPlayer(PlayerManager.Instance.PlayerName, PawnType.eRed, PlayerType.ePlayer, 1);

            IPlayer playerYellow = new ComputerPlayer();
            playerYellow.SetUpPlayer("Computer_1", PawnType.eYellow, PlayerType.eComputer, 2);


            players.Add(playerRed);
            players.Add(playerYellow);


        }
        else
        {
            IPlayer playerRed = new LocalPlayer();
            playerRed.SetUpPlayer(PlayerManager.Instance.PlayerName, PawnType.eRed, PlayerType.ePlayer, 1);

            IPlayer playerGreen = new ComputerPlayer();
            playerGreen.SetUpPlayer("Computer_2", PawnType.eGreen, PlayerType.eComputer, 2);


            IPlayer playerYellow = new ComputerPlayer();
            playerYellow.SetUpPlayer("Computer_3", PawnType.eYellow, PlayerType.eComputer, 3);

            IPlayer playerBlue = new ComputerPlayer();
            playerBlue.SetUpPlayer("Computer_4", PawnType.eBlue, PlayerType.eComputer, 4);

            players.Add(playerRed);
            players.Add(playerGreen);
            players.Add(playerYellow);
            players.Add(playerBlue);

        }


        m_GameTurnManager.SetUpGamePlay(players, GameMode.eComputerMode);
    }


    void CreateGlobalModePlayers()
    {
        if (PhotonNetwork.IsConnected)
        {
            Dictionary<int, Player> players = PhotonNetwork.CurrentRoom.Players;

            List<KeyValuePair<int, string>> turns = new List<KeyValuePair<int, string>>();

            foreach (KeyValuePair<int, Player> player in players)
            {
                Debug.Log("Player " + player.Key + " is " + player.Value);
                turns.Add(new KeyValuePair<int, string>(player.Key, player.Value.NickName));
            }

            List<IPlayer> playerList = new List<IPlayer>();

            KeyValuePair<int, string> data;

            if (m_MaxPlayers == 2)
            {
                IPlayer playerRed = new OnlinePlayer();
                data = turns[0];
                playerRed.SetUpPlayer(data.Value, PawnType.eRed, PlayerType.ePlayer, data.Key);

                IPlayer playerYellow = new OnlinePlayer();
                data = turns[1];
                playerYellow.SetUpPlayer(data.Value, PawnType.eYellow, PlayerType.eOnline, data.Key);


                playerList.Add(playerRed);
                playerList.Add(playerYellow);
            }
            else
            {
                IPlayer playerRed = new LocalPlayer();
                data = turns[0];
                playerRed.SetUpPlayer(data.Value, PawnType.eRed, PlayerType.ePlayer, data.Key);

                IPlayer playerGreen = new OnlinePlayer();
                data = turns[1];
                playerGreen.SetUpPlayer(data.Value, PawnType.eGreen, PlayerType.eOnline, data.Key);


                IPlayer playerYellow = new OnlinePlayer();
                data = turns[2];
                playerYellow.SetUpPlayer(data.Value, PawnType.eYellow, PlayerType.eOnline, data.Key);

                IPlayer playerBlue = new OnlinePlayer();
                data = turns[3];
                playerBlue.SetUpPlayer(data.Value, PawnType.eBlue, PlayerType.eOnline, data.Key);

                playerList.Add(playerRed);
                playerList.Add(playerGreen);
                playerList.Add(playerYellow);
                playerList.Add(playerBlue);

            }

            m_GameTurnManager.SetUpGamePlay(playerList, GameMode.eGlobalMode);

        }
    }


    public void OnClickLobby()
    {
        if (m_SelectedMode == GameMode.eGlobalMode)
        {
            GamePhotonViewManager.Instance.photonView.RPC("LobbyClicked", Photon.Pun.RpcTarget.All);
        }
        else
        {
            m_GameTurnManager.OnGameOver();
        }

    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Debug.Log("Player Left Room " + otherPlayer.NickName);

        PhotonNetwork.LoadLevel(0);
    }


}
