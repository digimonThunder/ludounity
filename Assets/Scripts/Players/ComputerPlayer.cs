﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerPlayer : MonoBehaviour, IPlayer
{
    private PawnType m_PawnType;

    private PlayerType m_PlayerType;

    private BoardHouseController m_HouseController;

    private int m_PlayerTurn;

    private string m_PlayerName;

    public void SetUpPlayer(string inPlayerName, PawnType inPawnType, PlayerType inPlayerType, int inTurn)
    {
        m_PawnType = inPawnType;

        m_PlayerType = inPlayerType;

        m_HouseController = GameTurnManager.Instance.Board.GetHouseControllerByType(m_PawnType);

        m_PlayerTurn = inTurn;

        m_PlayerName = inPlayerName;

    }


    public PawnType GetSelectedPawnType()
    {
        return m_PawnType;
    }

    public PlayerType GetPlayerType()
    {
        return m_PlayerType;

    }

    bool alterNate = false;


    public int DiceRolled()
    {
        alterNate = !alterNate;

        int diceValue = alterNate ? 6 : UnityEngine.Random.Range(1, 7);

        Debug.Log("Computer Dice Rolled --> " + diceValue);

        GameTurnManager.Instance.Board.StartCoroutine(delayedDiceRoll(diceValue));

        return diceValue;
    }

    IEnumerator delayedDiceRoll(int diceValue)
    {
        yield return new WaitForSeconds(0.5f);

        m_HouseController.Dice.BotRollDice(diceValue);
    }

    public int PawnSelected(int inDiceValue)
    {
        bool selected = false;

        if (inDiceValue == 6)
        {
            selected = TrySelectingFromLockedPawns();
        }

        if (!selected)
        {
            selected = TrySelectingFromUnlockedPawns(inDiceValue);
        }

        GameTurnManager.Instance.Board.StartCoroutine(delayedPawnSelectedDiceRoll());

        return m_SelectedPawn.BoardSlotIndex;
    }

    IEnumerator delayedPawnSelectedDiceRoll()
    {
        yield return new WaitForSeconds(1);

        GameEventManager.Instance.TriggerPawnSelected(m_SelectedPawn);
    }

    PawnController m_SelectedPawn = null;


    private bool TrySelectingFromLockedPawns()
    {
        bool retVal = false;

        List<PawnController> insideHouse = m_HouseController.PawnsInsideHouse;

        if (insideHouse.Count > 0)
        {
            int random = UnityEngine.Random.RandomRange(0, insideHouse.Count);

            m_SelectedPawn = insideHouse[random];

            retVal = true;

        }

        return retVal;
    }

    private bool TrySelectingFromUnlockedPawns(int diceValue)
    {
        bool retVal = false;

        List<PawnController> outSidePawns = m_HouseController.PawnsOutsideHouse;

        List<PawnController> possible = new List<PawnController>();


        for (int k = 0; k < outSidePawns.Count; k++)
        {
            if (outSidePawns[k].CanMoveForwardBy(diceValue))
            {
                possible.Add(outSidePawns[k]);

                retVal = true;

            }
            else
            {

            }


        }

        if (possible.Count > 0)
        {
            int random = UnityEngine.Random.RandomRange(0, possible.Count);

            m_SelectedPawn = possible[random];
        }


        return retVal;
    }



    public int GetPlayerTurn()
    {
        return m_PlayerTurn;
    }

    public string GetPlayerName()
    {
        return m_PlayerName;
    }
}
