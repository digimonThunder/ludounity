﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlinePlayer : IPlayer
{
    private PawnType m_PawnType;

    private PlayerType m_PlayerType;

    private int m_PlayerTurn;

    private string m_PlayerName;



    public void SetUpPlayer(string inPlayerName, PawnType inPawnType, PlayerType inPlayerType, int inTurn)
    {
        m_PawnType = inPawnType;

        m_PlayerType = inPlayerType;

        m_PlayerTurn = inTurn;
        m_PlayerName = inPlayerName;

    }

    public PawnType GetSelectedPawnType()
    {
        return m_PawnType;
    }

    public PlayerType GetPlayerType()
    {
        return m_PlayerType;

    }

    public int DiceRolled()
    {
        return -1;
    }

    public int PawnSelected(int inDiceValue)
    {
        return -1;
    }

    public int GetPlayerTurn()
    {
        return m_PlayerTurn;
    }

    public void TurnChanged(int inNextTurn)
    {

    }

    public string GetPlayerName()
    {
        return m_PlayerName;
    }
}
