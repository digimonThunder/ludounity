﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class GamePhotonViewManager : MonoBehaviourPun
{
    private static GamePhotonViewManager s_GlobalRef;

    public static GamePhotonViewManager Instance
    {
        get
        {
            return s_GlobalRef;
        }
    }

    public bool m_GameEnded = false;

    void Awake()
    {
        if (s_GlobalRef == null)
        {
            s_GlobalRef = this;

        }
        else
        {
            Destroy(gameObject);
        }
    }


    [PunRPC]
    void OnlineStartGame(int inStartingTurn)
    {
        GameTurnManager.Instance.StarGame(inStartingTurn);
    }


    [PunRPC]
    void TurnChanged(int inNextTurn)
    {
        GameTurnManager.Instance.OnTurnChanged(inNextTurn);
    }

    [PunRPC]
    void OnDiceRolled(int inDiceValue)
    {
        GameTurnManager.Instance.DiceRolled(inDiceValue);
    }

    [PunRPC]
    void OnPawnMoveCompleted(bool inStatus)
    {
        GameTurnManager.Instance.PawnMoveCompleted(inStatus);
    }


    [PunRPC]
    void OnPawnSelected(int inSlotIdentity, int inDiceValue)
    {
        GameTurnManager.Instance.PawnSelected(inSlotIdentity, inDiceValue);
    }

    [PunRPC]
    void OnGameOver()
    {
        GameTurnManager.Instance.OnGameOver();
    }

    [PunRPC]
    void OnGameEnded(bool inStatus)
    {
        m_GameEnded = inStatus;
    }

     [PunRPC]
    void LobbyClicked()
    {
        GameTurnManager.Instance.OnGameOver();
    }

}
