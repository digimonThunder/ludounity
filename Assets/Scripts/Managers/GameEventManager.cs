﻿using UnityEngine;

public class GameEventManager
{
    public delegate void GameEvent();

    public delegate void GameEventFloat(float inValue);

    public delegate void GameEventInt(int inValue);

    public delegate void GameEventBool(bool inValue);

    public delegate void GameEventString(string inValue);


    public delegate void DiceRolledEvent(PawnType inPawnType, int inDiceValue);

    public delegate void PawnSelectedEvent(PawnController inPawn);

    //-----------------------

    public event GameEventBool EventOnPawnMoveCompleted;

    public event DiceRolledEvent EventOnDiceRolled;

    public event PawnSelectedEvent EventPawnSelected;

    //-----------------------

    private static GameEventManager sGlobalRef = null;

    public static GameEventManager Instance
    {
        get
        {
            if (sGlobalRef == null)
            {
                sGlobalRef = new GameEventManager();
            }

            return sGlobalRef;
        }
    }

    private GameEventManager()
    {

    }

    public void TriggerDiceRolled(PawnType inType, int inDiceValue)
    {
        if (EventOnDiceRolled != null)
        {
            EventOnDiceRolled.Invoke(inType, inDiceValue);
        }
    }

    public void TriggerPawnSelected(PawnController inPawn)
    {
        if (EventPawnSelected != null)
        {
            EventPawnSelected.Invoke(inPawn);
        }
    }

    public void TriggerPawnMoveCompleted(bool inBonusTurn)
    {
        if (EventOnPawnMoveCompleted != null)
        {
            EventOnPawnMoveCompleted.Invoke(inBonusTurn);
        }
    }

    public void Destroy()
    {
        sGlobalRef = null;
    }

}
