﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PawnType
{
    eRed,
    eGreen,
    eYellow,
    eBlue
}


[Serializable]
public class PathData
{
    public BoardCell[] m_NormalCommonPath;

    public BoardCell[] m_RedFinishPath;

    public BoardCell[] m_GreenFinishPath;

    public BoardCell[] m_YellowFinishPath;

    public BoardCell[] m_BlueFinishPath;
}


public class BoardController : MonoBehaviour
{
    [SerializeField]
    private PathData m_BoardPath;

    [SerializeField]
    private BoardHouseController[] m_BoardHouses;

    private Dictionary<PawnType, List<BoardCell>> m_PawnPathByType;


    void Awake()
    {
        GeneratePathForAllPawns();
    }

    void GeneratePathForAllPawns()
    {
        m_PawnPathByType = new Dictionary<PawnType, List<BoardCell>>();

        m_PawnPathByType[PawnType.eRed] = GenerateBoardCellPathByType(PawnType.eRed);
        m_PawnPathByType[PawnType.eBlue] = GenerateBoardCellPathByType(PawnType.eBlue);
        m_PawnPathByType[PawnType.eGreen] = GenerateBoardCellPathByType(PawnType.eGreen);
        m_PawnPathByType[PawnType.eYellow] = GenerateBoardCellPathByType(PawnType.eYellow);

    }

    private List<BoardCell> GenerateBoardCellPathByType(PawnType inType)
    {
        List<BoardCell> retVal = new List<BoardCell>();

        int index = 0;

        int totalNormalPathCellCount = m_BoardPath.m_NormalCommonPath.Length;

        int maxBreakCount = totalNormalPathCellCount * 2;

        int breakLoop = 0;

        bool done = false;

        bool startingPointFound = false;

        BoardCellType startPoint, endPoint;

        SetStartAndEndPointForPawn(inType, out startPoint, out endPoint);

        while (!done && breakLoop++ < maxBreakCount)
        {
            BoardCell boardCell = m_BoardPath.m_NormalCommonPath[index];

            boardCell.gameObject.name = index.ToString();

            if (!startingPointFound)
            {
                if (boardCell.BoardCellType == startPoint)
                {
                    startingPointFound = true;
                    retVal.Add(boardCell);
                }
            }
            else
            {
                retVal.Add(boardCell);
            }

            index = (++index) % totalNormalPathCellCount;

            if (boardCell.BoardCellType == endPoint && startingPointFound)
            {
                done = true;
            }
        }

        BoardCell[] finishPathPonts = GetFinishPathByType(inType);


        for (int i = 0; i < finishPathPonts.Length; i++)
        {
            retVal.Add(finishPathPonts[i]);
        }

        return retVal;

    }

    private BoardCell[] GetFinishPathByType(PawnType inType)
    {
        BoardCell[] finishPathPonts = null;

        switch (inType)
        {
            case PawnType.eRed:

                finishPathPonts = m_BoardPath.m_RedFinishPath;

                break;

            case PawnType.eBlue:

                finishPathPonts = m_BoardPath.m_BlueFinishPath;

                break;

            case PawnType.eGreen:

                finishPathPonts = m_BoardPath.m_GreenFinishPath;

                break;

            case PawnType.eYellow:

                finishPathPonts = m_BoardPath.m_YellowFinishPath;

                break;
        }

        return finishPathPonts;

    }

    private void SetStartAndEndPointForPawn(PawnType inPawnType, out BoardCellType outStartPoint, out BoardCellType outEndPoint)
    {
        switch (inPawnType)
        {
            case PawnType.eRed:

                outStartPoint = BoardCellType.eRedStartCell;

                outEndPoint = BoardCellType.eRedEndCell;

                break;

            case PawnType.eGreen:

                outStartPoint = BoardCellType.eGreenStartCell;

                outEndPoint = BoardCellType.eGreenEndCell;

                break;

            case PawnType.eBlue:

                outStartPoint = BoardCellType.eBlueStartCell;

                outEndPoint = BoardCellType.eBlueEndCell;

                break;

            case PawnType.eYellow:

                outStartPoint = BoardCellType.eYellowStartCell;

                outEndPoint = BoardCellType.eYellowEndCell;

                break;

            default:

                outStartPoint = BoardCellType.eYellowStartCell;

                outEndPoint = BoardCellType.eYellowEndCell;

                break;


        }
    }

    public void SetUpBoardFor(List<IPlayer> inPlayers)
    {
        //TODO to rotate board based on player selected pawn and choose cross indexes for two player
        for (int i = 0; i < inPlayers.Count; i++)
        {
            GetHouseControllerByType(inPlayers[i].GetSelectedPawnType()).SetUpHouse(inPlayers[i].GetPlayerName());
        }
    }

    public List<BoardCell> GetPawnTrackByType(PawnType inPawnType)
    {
        List<BoardCell> retVal = null;

        m_PawnPathByType.TryGetValue(inPawnType, out retVal);

        return retVal;
    }

    public BoardHouseController GetHouseControllerByType(PawnType inPawnType)
    {
        BoardHouseController retVal = null;

        for (int j = 0; j < m_BoardHouses.Length; j++)
        {
            if (inPawnType == m_BoardHouses[j].CurrentPawnType)
            {
                retVal = m_BoardHouses[j];

                break;
            }
        }

        return retVal;
    }
}
