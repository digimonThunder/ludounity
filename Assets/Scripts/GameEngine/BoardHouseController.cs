﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerType
{
    eComputer,
    ePlayer,
    eOnline
}

[Serializable]
public class HouseSlot
{
    public bool m_Occupied;

    public Transform m_SlotPosition;
}

public class BoardHouseController : MonoBehaviour
{
    [SerializeField]
    private PawnType m_PawnType;

    [SerializeField]
    private DiceController m_Dice;

    [SerializeField]
    private GameObject m_Highlighter;

    [SerializeField]
    private TMPro.TextMeshPro m_PlayerName;

    [SerializeField]
    private HouseSlot[] m_BoardSlots;


    private List<PawnController> m_PawnsInsideHouse = new List<PawnController>();

    private List<PawnController> m_PawnsOutsideHouse = new List<PawnController>();

    private int m_LastDiceValue = 0;


    public PawnType CurrentPawnType
    {
        get
        {
            return m_PawnType;
        }
    }

    public List<PawnController> PawnsOutsideHouse
    {
        get
        {
            return m_PawnsOutsideHouse;
        }
    }

    public List<PawnController> PawnsInsideHouse
    {
        get
        {
            return m_PawnsInsideHouse;
        }
    }

    public int RemainingPawnsInsideHouse
    {
        get
        {
            return m_PawnsInsideHouse.Count;
        }
    }

    public DiceController Dice
    {
        get
        {
            return m_Dice;
        }
    }

    public bool AllPawnsFinished
    {
        get
        {
            bool retVal = RemainingPawnsInsideHouse <= 0;

            if (retVal)
            {
                for (int i = 0; i < m_PawnsOutsideHouse.Count; i++)
                {
                    if (m_PawnsOutsideHouse[i].CurrentPawnState == PawnState.eFinish)
                    {
                        retVal = true;
                    }
                    else
                    {
                        retVal = false;
                        break;
                    }
                }
            }

            return retVal;
        }
    }

    public void HighlightHouseStatus(bool inStatus)
    {
        m_Highlighter.SetActive(inStatus);
    }


    public void SetUpHouse(string inPlayerName)
    {
        GameObject holder = new GameObject(m_PawnType.ToString() + "_PawnHolder");

        for (int i = 0; i < 4; i++)
        {
            GameObject pawn = Instantiate(GameAssetController.Instance.PawnPrefab, m_BoardSlots[i].m_SlotPosition.position, Quaternion.identity, holder.transform) as GameObject;

            PawnController pawnContr = pawn.GetComponent<PawnController>();

            pawn.name = m_PawnType.ToString() + (i + 1);

            pawnContr.SetUpPawn(m_PawnType, i);

            m_PawnsInsideHouse.Add(pawnContr);

            m_BoardSlots[i].m_Occupied = true;

            m_PlayerName.text = inPlayerName;

            m_PlayerName.enabled = true;
        }

        m_Dice.SetUp(m_PawnType);
    }

    private void UnlockPawnFromHouse(PawnController inPawn)
    {
        if (RemainingPawnsInsideHouse > 0)
        {
            int index = m_PawnsInsideHouse.FindIndex(0, m_PawnsInsideHouse.Count, (x => x == inPawn));

            m_PawnsInsideHouse.Remove(inPawn);

            m_BoardSlots[inPawn.BoardSlotIndex].m_Occupied = false;

            m_PawnsOutsideHouse.Add(inPawn);

            inPawn.UnlockPawn();
        }
    }

    private void LockPawnToHouse(PawnController inPawnController)
    {
        if (RemainingPawnsInsideHouse < 4)
        {
            m_PawnsOutsideHouse.Remove(inPawnController);

            m_PawnsInsideHouse.Add(inPawnController);

            HouseSlot slot = GetUnOccupiedSlot();

            inPawnController.CureentBoardCell.UnRegisterAllPawnFromCell();

            slot.m_Occupied = true;

            inPawnController.transform.position = slot.m_SlotPosition.position;

            inPawnController.LockPawn();

            //animate pawn from outise to inside

        }

    }

    public HouseSlot GetUnOccupiedSlot()
    {
        HouseSlot retVal = null;

        for (int i = 0; i < 4; i++)
        {
            if (m_BoardSlots[i].m_Occupied)
            {

            }
            else
            {
                retVal = m_BoardSlots[i];
                break;
            }
        }

        return retVal;
    }


    public void UpdateDiceActiveState(bool inStatus)
    {
        m_Dice.CanDice = inStatus;
    }

    public bool CheckIfCanMakeMove(int inDiceValue, bool inInputstatus)
    {
        bool retVal = false;

        m_LastDiceValue = inDiceValue;


        bool didLockedPawnActivated = false;
        bool didUnLockedPawnActivated = false;


        if (inDiceValue == 6)
        {
            if (m_PawnsInsideHouse.Count > 0)
            {
                didLockedPawnActivated = UpdateSelectionStateOfLockedPawns(inInputstatus);
            }
        }

        if (m_PawnsOutsideHouse.Count > 0)
        {
            didUnLockedPawnActivated = UpdateSelectionStateOfUnlockedPawns(inInputstatus);
        }

        retVal = didLockedPawnActivated || didUnLockedPawnActivated;

        CheckShouldByPass(didLockedPawnActivated, didUnLockedPawnActivated);

        return retVal;

    }

    private bool UpdateSelectionStateOfLockedPawns(bool inStatus)
    {
        bool retVal = false;

        for (int k = 0; k < m_PawnsInsideHouse.Count; k++)
        {
            m_PawnsInsideHouse[k].UpdateTapStatus(inStatus);

            retVal = true;
        }

        return retVal;
    }

    private bool UpdateSelectionStateOfUnlockedPawns(bool inStatus)
    {
        bool retVal = false;

        for (int k = 0; k < m_PawnsOutsideHouse.Count; k++)
        {
            if (m_PawnsOutsideHouse[k].CanMoveForwardBy(m_LastDiceValue))
            {
                m_PawnsOutsideHouse[k].UpdateTapStatus(inStatus);

                retVal = true;
            }
        }

        return retVal;
    }

    private void CheckShouldByPass(bool didLockedPawnActivated, bool didUnLockedPawnActivated)
    {
        if (didLockedPawnActivated || didUnLockedPawnActivated)
        {
            //do not bypass as there some pawns which can be moved 
        }
        else
        {
            //no pawns to move for the dice value 
            GameEventManager.Instance.TriggerPawnMoveCompleted(false);

        }
    }

    public void OnUserSelectedPawn(int inSlotIdentity, int inDiceValue)
    {
        PawnController pawn = m_PawnsInsideHouse.Find(x => x.BoardSlotIndex == inSlotIdentity);

        if (pawn == null)
        {
            pawn = m_PawnsOutsideHouse.Find(x => x.BoardSlotIndex == inSlotIdentity);
        }

        if (pawn != null)
        {
            OnPawnSelected(pawn, inDiceValue);
        }

    }


    private void OnPawnSelected(PawnController inPawn, int inDiceValue)
    {
        if (m_PawnType == inPawn.PawnType)
        {
            UpdateSelectionStateOfUnlockedPawns(false);

            UpdateSelectionStateOfLockedPawns(false);

            if (inPawn.CurrentPawnState == PawnState.eLocked)
            {
                UnlockPawnFromHouse(inPawn);

                GameEventManager.Instance.TriggerPawnMoveCompleted(inDiceValue == 6);

            }
            else
            {
                inPawn.MoveForwardBy(inDiceValue);

                CheckForKillsAtNewCell(inPawn);
            }

        }

    }

    void CheckForKillsAtNewCell(PawnController inPawn)
    {
        bool didKill = false;

        if (inPawn != null && inPawn.CurrentPawnState == PawnState.eUnlocked)
        {
            if (inPawn.CureentBoardCell.IsSafeCell)
            {

            }
            else
            {
                List<PawnController> listOfPawnInCell = inPawn.CureentBoardCell.PawnsInCell;

                for (int j = 0; j < listOfPawnInCell.Count; j++)
                {
                    PawnController pawn = listOfPawnInCell[j];

                    if (pawn.PawnType != inPawn.PawnType)
                    {
                        BoardHouseController house = GameTurnManager.Instance.Board.GetHouseControllerByType(pawn.PawnType);

                        house.LockPawnToHouse(pawn);

                        didKill = true;
                    }
                }
            }
        }

        bool canGiveBonusMove = m_LastDiceValue == 6 || didKill || inPawn.CurrentPawnState == PawnState.eFinish;

        GameEventManager.Instance.TriggerPawnMoveCompleted(canGiveBonusMove);

    }

}
