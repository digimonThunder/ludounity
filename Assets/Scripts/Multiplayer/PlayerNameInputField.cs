﻿using UnityEngine;
using UnityEngine.UI;


using Photon.Pun;
using Photon.Realtime;


using System.Collections;



/// <summary>
/// Player name input field. Let the user input his name, will appear above the player in the game.
/// </summary>
[RequireComponent(typeof(TMPro.TMP_InputField))]
public class PlayerNameInputField : MonoBehaviour
{
    #region Private Constants


    // Store the PlayerPref Key to avoid typos
    const string playerNamePrefKey = "PlayerName";


    #endregion

    [SerializeField]
    private TMPro.TMP_InputField m_InputField;


    #region MonoBehaviour CallBacks

    void Start()
    {
        SetUp();
    }


    /// <summary>
    /// MonoBehaviour method called on GameObject by Unity during initialization phase.
    /// </summary>
    public void SetUp()
    {

        string defaultName = "Guest_" + System.DateTime.Now.Millisecond;


        PhotonNetwork.NickName = defaultName;

        m_InputField.text = defaultName;

        PlayerManager.Instance.PlayerName = defaultName;


        Debug.Log("Player Name: " + defaultName);
    }


    #endregion


    #region Public Methods


    /// <summary>
    /// Sets the name of the player, and save it in the PlayerPrefs for future sessions.
    /// </summary>
    /// <param name="value">The name of the Player</param>
    public void SetPlayerName(string value)
    {
        // #Important
        if (string.IsNullOrEmpty(value))
        {
            Debug.LogError("Player Name is null or empty");
            return;
        }
        PhotonNetwork.NickName = value;

        PlayerManager.Instance.PlayerName = value;


    }

    public void SetInputFieldStatus(bool inStatus)
    {
        m_InputField.interactable = inStatus;
    }


    #endregion
}
