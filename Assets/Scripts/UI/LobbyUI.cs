﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LobbyUI : MonoBehaviourPunCallbacks
{

    [SerializeField]
    private TMPro.TextMeshProUGUI m_PlayerConnectionStatus;


    [SerializeField]
    private Button m_OnlineModeButton;


    [SerializeField]
    private Button m_ComputerModeButton;


    [SerializeField]
    private Button m_LocalModeButton;


    [SerializeField]
    private GameObject m_SettingsPanel;


    [SerializeField]
    private Button m_MaxFourToggle;


    [SerializeField]
    private Button m_MaxTwoToggle;

    [SerializeField]
    private TMPro.TextMeshProUGUI m_MaxPlayerText;


    private string m_GameVersion = "1";

    private bool m_PlayerJoinedRoom;


    void Awake()
    {
        PhotonNetwork.Disconnect();

        PhotonNetwork.AutomaticallySyncScene = true;

        m_PlayerConnectionStatus.text = "Connecting...";

        OnClick(2);

    }

    void Start()
    {
        SetUpPhoton();
    }

    public void SetUpPhoton()
    {
        m_OnlineModeButton.interactable = false;

        // #Critical, we must first and foremost connect to Photon Online Server.
        PhotonNetwork.ConnectUsingSettings();
        PhotonNetwork.GameVersion = m_GameVersion;

    }


    private void ConnectGlobal()
    {
        if (PhotonNetwork.IsConnected)
        {
            m_PlayerJoinedRoom = false;

            m_OnlineModeButton.interactable = false;

            PhotonNetwork.JoinRandomRoom();
        }
    }

    public override void OnConnectedToMaster()
    {
        m_PlayerConnectionStatus.text = "Connection Success";

        Debug.Log("Photon Network Connection Successful");

        m_OnlineModeButton.interactable = true;

    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("Photon Network Connection Succeeful with problem: " + cause);

        m_PlayerConnectionStatus.text = "Connection Failed: " + cause.ToString();

        m_OnlineModeButton.interactable = false;

        SetUpPhoton();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        m_PlayerJoinedRoom = false;

        Debug.Log("Photon Network OnJoinRandomFailed " + message);

        // #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
        PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = (byte)PlayerManager.Instance.MaxPlayers });
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom Successful");

        m_PlayerConnectionStatus.text = "Joined Room";

        m_OnlineModeButton.interactable = false;

        m_PlayerJoinedRoom = true;

    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        m_PlayerConnectionStatus.text = newPlayer.NickName;

        if (PhotonNetwork.IsMasterClient)
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount == (byte)PlayerManager.Instance.MaxPlayers)
            {
                PhotonNetwork.CurrentRoom.IsOpen = false;

                Debug.Log("All Player Entered--> Load Game");

                m_PlayerConnectionStatus.text = "All Player Entered";

                PhotonNetwork.LoadLevel(1);

            }
        }
    }

    void Update()
    {
        if (m_PlayerJoinedRoom)
        {
            m_PlayerConnectionStatus.text = "Waiting For Players";

        }
    }

    public void OnClickModes(int inMode)
    {

        switch (inMode)
        {
            case 2:

                PlayerManager.Instance.SelectedGameMode = GameMode.eLocalMode;

                SceneManager.LoadScene(1);

                break;

            case 1:

                PlayerManager.Instance.SelectedGameMode = GameMode.eComputerMode;

                SceneManager.LoadScene(1);


                break;

            case 0:

                m_LocalModeButton.interactable = false;

                m_ComputerModeButton.interactable = false;

                PlayerManager.Instance.SelectedGameMode = GameMode.eGlobalMode;

                ConnectGlobal();

                break;
        }
    }

    public void OnClickPawns(PawnType inPawn)
    {
        PlayerManager.Instance.SelectedPawn = inPawn;
    }

    public void OnClickSettingButton()
    {
        m_SettingsPanel.SetActive(!m_SettingsPanel.active);
    }

    bool m_TwoToggleState;
    public void OnClick(int inValue)
    {
        if (inValue == 4)
        {
            m_MaxTwoToggle.interactable = true;
            m_MaxFourToggle.interactable = false;

            PlayerManager.Instance.MaxPlayers = 4;

            m_MaxPlayerText.text = "4 Players";
        }
        else
        {
            m_MaxTwoToggle.interactable = false;
            m_MaxFourToggle.interactable = true;

            PlayerManager.Instance.MaxPlayers = 2;

            m_MaxPlayerText.text = "2 Players";


        }

    }

}