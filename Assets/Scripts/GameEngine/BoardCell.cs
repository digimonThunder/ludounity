﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BoardCellType
{
    eDefault,
    eRedStartCell,
    eRedEndCell,
    eGreenStartCell,
    eGreenEndCell,
    eYellowStartCell,
    eYellowEndCell,
    eBlueStartCell,
    eBlueEndCell
}

public class BoardCell : MonoBehaviour
{
    [SerializeField]
    private BoardCellType m_BoardCellType;

    [SerializeField]
    private bool m_IsSafeCell;

    private List<PawnController> m_PawnControllers = new List<PawnController>();

    public bool IsSafeCell
    {
        get
        {
            return m_IsSafeCell;
        }
    }

    public List<PawnController> PawnsInCell
    {
        get
        {
            return m_PawnControllers;
        }
    }

    public BoardCellType BoardCellType
    {
        get
        {
            return m_BoardCellType;
        }
    }

    public void RegisterPawnToCell(PawnController inPawnController)
    {
        if (!m_PawnControllers.Contains(inPawnController))
        {
            m_PawnControllers.Add(inPawnController);
        }
    }

    public void UnRegisterPawnFromCell(PawnController inPawnController)
    {
        if (m_PawnControllers.Count > 0)
        {
            m_PawnControllers.Remove(inPawnController);
        }
    }

    public void UnRegisterAllPawnFromCell()
    {
        if (m_PawnControllers.Count > 0)
        {
            m_PawnControllers.RemoveAll(x => x != null);
        }
    }

}