﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayer
{
    void SetUpPlayer(string inPlayerName, PawnType inPawnType, PlayerType inPlayerType, int inTurn);

    PawnType GetSelectedPawnType();

    PlayerType GetPlayerType();

    int GetPlayerTurn();

    int DiceRolled();

    int PawnSelected(int inDiceValue);

    string GetPlayerName();

}
