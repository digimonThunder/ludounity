﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public enum GameMode
{
    eLocalMode,
    eGlobalMode,
    eComputerMode
}

public class GameTurnManager : MonoBehaviour
{
    private PawnType m_CurrentPlayerPawnType;

    private List<IPlayer> m_Players;

    private GameMode m_GameMode = GameMode.eLocalMode;


    private List<int> m_PlayerTurns = new List<int>();

    private int m_CurrentTurnOrder;

    private int m_CurrentTurn;

    private static GameTurnManager s_GlobalRef;

    [SerializeField]
    private GameSceneManager m_GameSceneManager;

    [SerializeField]
    private BoardController m_BoardController;


    public BoardController Board
    {
        get
        {
            return m_BoardController;
        }
    }



    public static GameTurnManager Instance
    {
        get
        {
            return s_GlobalRef;
        }
        set
        {
            s_GlobalRef = value;
        }
    }


    private bool CanAllowInput
    {
        get
        {
            bool retVal = false;

            switch (m_GameMode)
            {
                case GameMode.eLocalMode:

                    retVal = true;

                    break;

                case GameMode.eComputerMode:

                    retVal = m_CurrentTurnOrder == 0;

                    break;


                case GameMode.eGlobalMode:

                    retVal = m_CurrentTurn == m_PlayerTurns[0];

                    break;

            }

            return retVal;
        }
    }


    public void SetUpGamePlay(List<IPlayer> inPlayers, GameMode inMode)
    {
        m_GameMode = inMode;

        m_Players = inPlayers;

        m_BoardController.SetUpBoardFor(m_Players);

        GameEventManager.Instance.EventOnDiceRolled -= OnDiceRolled;
        GameEventManager.Instance.EventOnDiceRolled += OnDiceRolled;

        GameEventManager.Instance.EventOnPawnMoveCompleted -= OnPawnMoveCompleted;
        GameEventManager.Instance.EventOnPawnMoveCompleted += OnPawnMoveCompleted;

        GameEventManager.Instance.EventPawnSelected -= OnPawnSelected;
        GameEventManager.Instance.EventPawnSelected += OnPawnSelected;

        inPlayers.ForEach(x =>
        {

            m_PlayerTurns.Add(x.GetPlayerTurn());

        });

        if (m_GameMode != GameMode.eGlobalMode)
        {
            StarGame(m_PlayerTurns[0]);
        }
        else if (Photon.Pun.PhotonNetwork.IsMasterClient)
        {
            GamePhotonViewManager.Instance.photonView.RPC("OnlineStartGame", Photon.Pun.RpcTarget.All, 1);
        }

    }

    public void StarGame(int inStartingTurn)
    {
        m_CurrentTurnOrder = m_PlayerTurns.IndexOf(inStartingTurn);

        m_CurrentTurn = m_PlayerTurns[m_CurrentTurnOrder];

        m_CurrentPlayerPawnType = GetPlayerByTurn(m_CurrentTurn).GetSelectedPawnType();

        ActivateGameForCurrentPlayer();

    }

    private IPlayer GetPlayerByTurn(int inTurn)
    {
        IPlayer player = null;

        if (m_Players != null)
        {
            player = m_Players.Find(x => x.GetPlayerTurn() == inTurn);
        }

        return player;
    }


    private void ChangeTurn()
    {
        m_CurrentTurnOrder = (m_CurrentTurnOrder + 1) % m_Players.Count;

        int nextTurn = m_PlayerTurns[m_CurrentTurnOrder];

        IPlayer player = GetPlayerByTurn(m_CurrentTurn);


        Debug.Log("ChangeTurn to " + nextTurn);


        switch (player.GetPlayerType())
        {
            case PlayerType.ePlayer:

                if (m_GameMode != GameMode.eGlobalMode)
                {
                    OnTurnChanged(nextTurn);
                }
                else
                {
                    OnTurnChanged(nextTurn);
                    GamePhotonViewManager.Instance.photonView.RPC("TurnChanged", Photon.Pun.RpcTarget.Others, nextTurn);
                }

                break;


            case PlayerType.eComputer:

                OnTurnChanged(nextTurn);

                break;
        }

    }


    private void ActivateGameForCurrentPlayer()
    {
        BoardHouseController houseController = m_BoardController.GetHouseControllerByType(m_CurrentPlayerPawnType);

        houseController.HighlightHouseStatus(true);

        IPlayer player = GetPlayerByTurn(m_CurrentTurn);

        PlayerNotifyTurnAndRollDice(player, houseController);
    }

    public void OnTurnChanged(int inTurn)
    {
        m_CurrentTurn = inTurn;

        m_CurrentTurnOrder = m_PlayerTurns.IndexOf(inTurn);

        m_CurrentPlayerPawnType = GetPlayerByTurn(m_CurrentTurn).GetSelectedPawnType();

        ActivateGameForCurrentPlayer();
    }


    void PlayerNotifyTurnAndRollDice(IPlayer inPlayer, BoardHouseController inHouseController)
    {
        if (inPlayer != null)
        {
            switch (inPlayer.GetPlayerType())
            {
                case PlayerType.ePlayer:

                    if (CanAllowInput)
                    {
                        inHouseController.UpdateDiceActiveState(true);
                    }

                    break;

                case PlayerType.eComputer:

                    int diceVal = inPlayer.DiceRolled(); //rolls dice 

                    break;

            }

        }

    }


    private void OnDiceRolled(PawnType inType, int inDiceValue)
    {
        IPlayer player = GetPlayerByTurn(m_CurrentTurn);

        if (m_GameMode != GameMode.eGlobalMode)
        {
            DiceRolled(inDiceValue);
        }
        else
        {
            if (player.GetPlayerType() == PlayerType.ePlayer)
            {
                GamePhotonViewManager.Instance.photonView.RPC("OnDiceRolled", Photon.Pun.RpcTarget.All, inDiceValue);
            }
            else
            {
                DiceRolled(inDiceValue);
            }
        }

    }

    private int m_DiceValue;
    public void DiceRolled(int inDiceValue)
    {
        // Debug.Log("XXX---> OnDiceRolled " + inDiceValue);

        m_DiceValue = inDiceValue;

        IPlayer player = GetPlayerByTurn(m_CurrentTurn);

        BoardHouseController houseController = m_BoardController.GetHouseControllerByType(m_CurrentPlayerPawnType);

        if (m_GameMode == GameMode.eGlobalMode)
        {
            StartCoroutine(houseController.Dice.UpdateDice(inDiceValue));

            if (player.GetPlayerType() == PlayerType.ePlayer)
            {
                RemoveCurrentHouseHighlight();
            }
        }
        else
        {
            RemoveCurrentHouseHighlight();
        }


        if (!houseController.AllPawnsFinished)
        {
            bool canMove = false;

            switch (player.GetPlayerType())
            {
                case PlayerType.ePlayer:

                    canMove = houseController.CheckIfCanMakeMove(inDiceValue, CanAllowInput);

                    break;


                case PlayerType.eComputer:

                    canMove = houseController.CheckIfCanMakeMove(inDiceValue, CanAllowInput);

                    if (canMove)
                    {
                        player.PawnSelected(inDiceValue);
                    }
                    else
                    {

                    }

                    break;
            }

        }
        else
        {
            Debug.Log("all pawns finished");
        }

    }

    private void OnPawnMoveCompleted(bool inStatus)
    {
        IPlayer player = GetPlayerByTurn(m_CurrentTurn);

        if (m_GameMode != GameMode.eGlobalMode)
        {
            PawnMoveCompleted(inStatus);
        }
        else
        {
            if (player.GetPlayerType() == PlayerType.ePlayer)
            {
                GamePhotonViewManager.Instance.photonView.RPC("OnPawnMoveCompleted", Photon.Pun.RpcTarget.All, inStatus);
            }
            else
            {
                PawnMoveCompleted(inStatus);
            }
        }
    }

    private void OnPawnSelected(PawnController inPawn)
    {
        IPlayer player = GetPlayerByTurn(m_CurrentTurn);

        if (m_GameMode != GameMode.eGlobalMode)
        {
            PawnSelected(inPawn.BoardSlotIndex, m_DiceValue);
        }
        else
        {
            if (player.GetPlayerType() == PlayerType.ePlayer)
            {
                GamePhotonViewManager.Instance.photonView.RPC("OnPawnSelected", Photon.Pun.RpcTarget.All, inPawn.BoardSlotIndex, m_DiceValue);
            }
            else
            {
                PawnSelected(inPawn.BoardSlotIndex, m_DiceValue);
            }
        }
    }

    void RemoveCurrentHouseHighlight()
    {
        BoardHouseController houseController = m_BoardController.GetHouseControllerByType(m_CurrentPlayerPawnType);

        houseController.HighlightHouseStatus(false);

    }

    public void PawnSelected(int inSlotIdentity, int inDiceValue)
    {
        BoardHouseController houseController = m_BoardController.GetHouseControllerByType(m_CurrentPlayerPawnType);

        houseController.OnUserSelectedPawn(inSlotIdentity, inDiceValue);

    }

    public void PawnMoveCompleted(bool inStatus)
    {
        BoardHouseController houseController = m_BoardController.GetHouseControllerByType(m_CurrentPlayerPawnType);

        IPlayer player = GetPlayerByTurn(m_CurrentTurn);

        if (m_GameMode == GameMode.eGlobalMode)
        {
            if (player.GetPlayerType() != PlayerType.ePlayer)
            {
                RemoveCurrentHouseHighlight();
            }
        }

        if (houseController.AllPawnsFinished)
        {
            Debug.Log("Game Finished");

            if (m_GameMode != GameMode.eGlobalMode)
            {
                OnGameOver();
            }
            else
            {
                if (player.GetPlayerType() == PlayerType.ePlayer)
                {
                    GamePhotonViewManager.Instance.photonView.RPC("OnGameOver", Photon.Pun.RpcTarget.All);
                }
                else
                {
                    OnGameOver();
                }
            }


        }
        else
        {
            if (inStatus)
            {
                ActivateGameForCurrentPlayer();
            }
            else
            {
                ChangeTurn();
            }
        }
    }

    public void OnGameOver()
    {
        if (m_GameMode == GameMode.eGlobalMode)
        {
            if (PhotonNetwork.IsConnected && PhotonNetwork.InRoom)
            {
                PhotonNetwork.AutomaticallySyncScene = false;

                PhotonNetwork.CurrentRoom.EmptyRoomTtl = 0;

                PhotonNetwork.LeaveRoom();

                Debug.Log("Player is leaving room");

            }
        }
        else
        {

        }

        UnityEngine.SceneManagement.SceneManager.LoadScene(0);

        s_GlobalRef = null;

        GameEventManager.Instance.Destroy();

        PlayerManager.Instance.Destroy();

    }
}
